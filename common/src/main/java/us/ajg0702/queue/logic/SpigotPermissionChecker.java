package us.ajg0702.queue.logic;

import us.ajg0702.queue.api.players.AdaptedPlayer;
import us.ajg0702.queue.api.queues.QueueServer;
import us.ajg0702.queue.api.server.AdaptedServer;
import us.ajg0702.queue.common.QueueMain;

import java.util.UUID;

public class SpigotPermissionChecker {
    public void checkPerm(QueueServer server, AdaptedPlayer player) {
        AdaptedServer idealServer = server.getIdealServer(player);
        QueueMain.getInstance().getPlatformMethods().sendPluginMessage(idealServer, "checkperm",
                player.getUniqueId().toString(), server.getName());
    }

    public void resPerm(String response) {
        String[] data = response.split(":");
        if (data.length != 2) return;

        String uuid = data[0];
        String name = data[1];

        QueueMain main = QueueMain.getInstance();
        AdaptedPlayer player = main.getPlatformMethods().getPlayer(UUID.fromString(uuid));
        QueueServer server = main.getQueueManager().findServer(name);
        if (server != null && player != null) {
            server.removePlayer(player);
            player.sendMessage(main.getMessages().getString("status.sending-now",
                    "SERVER:" + server.getAlias()));
            player.connect(server.getIdealServer(player));
        }
    }
}
